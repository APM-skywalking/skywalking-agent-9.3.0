/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.skywalking.apm.plugin.httpClient.v4;

import cn.hutool.core.collection.CollUtil;
import com.ctrip.framework.apollo.ConfigService;
import org.apache.http.*;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.EofSensorInputStream;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.skywalking.apm.agent.core.context.CarrierItem;
import org.apache.skywalking.apm.agent.core.context.ContextCarrier;
import org.apache.skywalking.apm.agent.core.context.ContextManager;
import org.apache.skywalking.apm.agent.core.context.tag.Tags;
import org.apache.skywalking.apm.agent.core.context.trace.AbstractSpan;
import org.apache.skywalking.apm.agent.core.context.trace.SpanLayer;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.EnhancedInstance;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.InstanceMethodsAroundInterceptor;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.MethodInterceptResult;
import org.apache.skywalking.apm.network.trace.component.ComponentsDefine;
import org.apache.skywalking.apm.util.StringUtil;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpClientExecuteInterceptor implements InstanceMethodsAroundInterceptor {
    private static final String ERROR_URI = "/_blank";

    @Override
    public void beforeMethod(EnhancedInstance objInst, Method method, Object[] allArguments, Class<?>[] argumentsTypes,
                             MethodInterceptResult result) throws Throwable {
        if (allArguments[0] == null || allArguments[1] == null) {
            // illegal args, can't trace. ignore.
            return;
        }
        final HttpHost httpHost = (HttpHost) allArguments[0];
        HttpRequest httpRequest = (HttpRequest) allArguments[1];
        final ContextCarrier contextCarrier = new ContextCarrier();

        String remotePeer = httpHost.getHostName() + ":" + port(httpHost);

        String uri = httpRequest.getRequestLine().getUri();
        String requestURI = getRequestURI(uri);
        String operationName = requestURI;
        AbstractSpan span = ContextManager.createExitSpan(operationName, contextCarrier, remotePeer);
        if (ERROR_URI.equals(requestURI)) {
            span.errorOccurred();
        }
        span.setComponent(ComponentsDefine.HTTPCLIENT);
        Tags.URL.set(span, buildSpanValue(httpHost, uri));
        Tags.HTTP.METHOD.set(span, httpRequest.getRequestLine().getMethod());
        SpanLayer.asHttp(span);

        CarrierItem next = contextCarrier.items();
        while (next.hasNext()) {
            next = next.next();
            httpRequest.setHeader(next.getHeadKey(), next.getHeadValue());
        }
        collectHttpParam(httpRequest, span);
    }

    @Override
    public Object afterMethod(EnhancedInstance objInst, Method method, Object[] allArguments, Class<?>[] argumentsTypes,
                              Object ret) throws Throwable {
        if (allArguments[0] == null || allArguments[1] == null) {
            return ret;
        }

        if (ret != null) {
            HttpResponse response = (HttpResponse) ret;
            StatusLine responseStatusLine = response.getStatusLine();
            if (responseStatusLine != null) {
                int statusCode = responseStatusLine.getStatusCode();
                AbstractSpan span = ContextManager.activeSpan();
                Tags.HTTP_RESPONSE_STATUS_CODE.set(span, statusCode);
                if (statusCode >= 400) {
                    span.errorOccurred();
                }
                HttpRequest httpRequest = (HttpRequest) allArguments[1];
                collectHttpResponse(response, span);
            }
        }

        ContextManager.stopSpan();
        return ret;
    }

    private static byte[] cloneInputStream(InputStream input) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = input.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
            baos.close();
            input.close();
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getInputStream(InputStream in) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        BufferedInputStream br = new BufferedInputStream(in);
        byte[] b = new byte[1024];
        for (int c = 0; (c = br.read(b)) != -1; ) {
            bos.write(b, 0, c);
        }
        b = null;
        br.close();


        in = new ByteArrayInputStream(bos.toByteArray());
        // 第一次读流
        StringBuffer out = new StringBuffer();
        byte[] b1 = new byte[1024];
        for (int n; (n = in.read(b1)) != -1; ) {
            out.append(new String(b1, 0, n));  //这个可以用来读取文件内容 并且文件内容有中文读取出来也不会乱码
        }
        String resultHtml = out.toString();
        in.reset();
        return resultHtml;
    }


    private static String convertStreamToString(InputStream in) throws IOException, IllegalAccessException, NoSuchFieldException {
        String inputStream = getInputStream(in);
        Field eofDetectedField = EofSensorInputStream.class.getDeclaredField("selfClosed");
        eofDetectedField.setAccessible(true); // 设置为可访问
        // 更新私有属性
        eofDetectedField.setBoolean(((EofSensorInputStream) in), false);
        return inputStream;
    }

    private void collectHttpResponse(HttpResponse httpResponse, AbstractSpan span) {
        try {
//            if (ConfigService.getAppConfig().getBooleanProperty("skywalking.agent.switch.collect.httpclient.response", true)) {
//                InputStream content = httpResponse.getEntity().getContent();
//                // 读取inputstream转换为string
//                String resp = convertStreamToString(content);
//                if (StringUtil.isNotBlank(resp)) {
//                    Tags.HTTP.RESPONSE.set(span, resp);
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleMethodException(EnhancedInstance objInst, Method method, Object[] allArguments,
                                      Class<?>[] argumentsTypes, Throwable t) {
        AbstractSpan activeSpan = ContextManager.activeSpan();
        activeSpan.log(t);
    }

    private String getRequestURI(String uri) {
        if (isUrl(uri)) {
            String requestPath;
            try {
                requestPath = new URL(uri).getPath();
            } catch (MalformedURLException e) {
                return ERROR_URI;
            }
            return requestPath != null && requestPath.length() > 0 ? requestPath : "/";
        } else {
            return uri;
        }
    }

    private boolean isUrl(String uri) {
        String lowerUrl = uri.toLowerCase();
        return lowerUrl.startsWith("http") || lowerUrl.startsWith("https");
    }

    private String buildSpanValue(HttpHost httpHost, String uri) {
        if (isUrl(uri)) {
            return uri;
        } else {
            StringBuilder buff = new StringBuilder();
            buff.append(httpHost.getSchemeName().toLowerCase());
            buff.append("://");
            buff.append(httpHost.getHostName());
            buff.append(":");
            buff.append(port(httpHost));
            buff.append(uri);
            return buff.toString();
        }
    }

    private int port(HttpHost httpHost) {
        int port = httpHost.getPort();
        return port > 0 ? port : "https".equals(httpHost.getSchemeName().toLowerCase()) ? 443 : 80;
    }

    private void collectHttpParam(HttpRequest httpRequest, AbstractSpan span) {
        if (ConfigService.getAppConfig().getBooleanProperty("skywalking.agent.switch.collect.httpclient.param", true)) {
            if (httpRequest instanceof HttpUriRequest) {
                URI uri = ((HttpUriRequest) httpRequest).getURI();
                if (StringUtil.isNotBlank(uri.getQuery())) {
                    Tags.HTTP.PARAMS.set(span, uri.getQuery());
                }

                // 如果是httpparam
                if (httpRequest.getParams() instanceof BasicHttpParams) {
                    BasicHttpParams params = (BasicHttpParams) httpRequest.getParams();
                    // 获取params的所有参数按照 XX=XX&XX=XX拼接
                    if (CollUtil.isNotEmpty( params.getNames())) {
                        StringBuilder paramsBuilder = new StringBuilder();

                        for (String param : params.getNames()) {

                            Object value = params.getParameter(param);
                            // 如果是最后一个不要拼接&
                            paramsBuilder.append(param).append("=").append(value).append("&");
                        }
                        String realParams = paramsBuilder.toString().substring(0, paramsBuilder.length() - 1);
                        Tags.HTTP.PARAMS.set(span, realParams);
                    }

                }
            }
        }

        if (httpRequest instanceof HttpPost) {
            HttpPost httpPost = (HttpPost) httpRequest;
            HttpEntity entity = httpPost.getEntity();
            if (entity != null) {
                try {
                    String requestBody = EntityUtils.toString(entity, StandardCharsets.UTF_8);
                    if (StringUtil.isNotEmpty(requestBody)) {
                        Tags.HTTP.BODY.set(span, requestBody);
                    }
                } catch (Exception e) {
                    // 处理异常
                    e.printStackTrace();
                }
            }
        }
    }
}

