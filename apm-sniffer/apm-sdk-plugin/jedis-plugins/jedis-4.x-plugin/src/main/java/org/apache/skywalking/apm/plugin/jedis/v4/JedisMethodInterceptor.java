/*
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements.  See the NOTICE file distributed with
 *   this work for additional information regarding copyright ownership.
 *   The ASF licenses this file to You under the Apache License, Version 2.0
 *   (the "License"); you may not use this file except in compliance with
 *   the License.  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package org.apache.skywalking.apm.plugin.jedis.v4;

import com.ctrip.framework.apollo.ConfigService;
import org.apache.skywalking.apm.agent.core.context.ContextManager;
import org.apache.skywalking.apm.agent.core.context.tag.StringTag;
import org.apache.skywalking.apm.agent.core.context.tag.Tags;
import org.apache.skywalking.apm.agent.core.context.trace.AbstractSpan;
import org.apache.skywalking.apm.agent.core.context.trace.SpanLayer;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.EnhancedInstance;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.InstanceMethodsAroundInterceptor;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.MethodInterceptResult;
import org.apache.skywalking.apm.network.trace.component.ComponentsDefine;

import java.lang.reflect.Method;
import java.util.Map;

import org.apache.skywalking.apm.util.StringUtil;

public class JedisMethodInterceptor implements InstanceMethodsAroundInterceptor {
    private static final StringTag TAG_ARGS = new StringTag("actual_target");

    @Override
    public void beforeMethod(EnhancedInstance objInst, Method method, Object[] allArguments, Class<?>[] argumentsTypes,
                             MethodInterceptResult result) throws Throwable {
        final ConnectionInformation connectionData = (ConnectionInformation) objInst.getSkyWalkingDynamicField();
        // Use cluster information to adapt Virtual Cache if exists, otherwise use real server host
        String peer =  StringUtil.isBlank(connectionData.getClusterNodes()) ? connectionData.getActualTarget() : connectionData.getClusterNodes();
        AbstractSpan span = ContextManager.createExitSpan("Jedis/" + method.getName(), peer);
        span.setComponent(ComponentsDefine.JEDIS);
        SpanLayer.asCache(span);
        Tags.CACHE_TYPE.set(span, "Redis");
        Tags.CACHE_CMD.set(span, "BATCH_EXECUTE");
        TAG_ARGS.set(span, connectionData.getActualTarget());

        // 采集jedis完整命令
        if (ConfigService.getAppConfig().getBooleanProperty("skywalking.agent.switch.collect.cache.completeCmd", true)) {
            // 操作命令
            StringBuilder stringBuilder = new StringBuilder();

            String name = method.getName();
            stringBuilder.append(name);
            stringBuilder.append(" ");

            byte[] allArgument = (byte[]) allArguments[0];
            String key = new String(allArgument);
            stringBuilder.append(key);
            stringBuilder.append(" ");
            for (int i = 1; i < allArguments.length; i++) {
                if (allArguments[i] instanceof byte[]) {
                    String s = new String((byte[]) allArguments[i]);
                    stringBuilder.append(s);
                    stringBuilder.append(" ");
                } else if (allArguments[i] instanceof Map) {
                    Map map = (Map) allArguments[i];
                    map.keySet().forEach(s -> {
                        stringBuilder.append(new String((byte[]) s) + " " + new String((byte[]) map.get(s)));

                        stringBuilder.append(" ");
                    });
                } else if (allArguments[i] instanceof byte[][]) {
                    for (byte[] bytes : (byte[][]) allArguments[i]) {
                        stringBuilder.append(new String(bytes));
                        stringBuilder.append(" ");
                    }
                }
            }
            String completeCmd = stringBuilder.toString();
            span.tag("cache.completeCmd", completeCmd);
        }
    }

    @Override
    public Object afterMethod(EnhancedInstance objInst, Method method, Object[] allArguments, Class<?>[] argumentsTypes,
                              Object ret) throws Throwable {

        // 是否采集缓存响应
        if (ConfigService.getAppConfig().getBooleanProperty("skywalking.agent.switch.collect.cache.result", true)) {
            String cacheResult;
            if (ret instanceof String) {
                cacheResult = (String) ret;
            } else if (ret instanceof byte[]) {
                cacheResult = new String((byte[]) ret);
            } else if (ret instanceof Long) {
                cacheResult = String.valueOf(ret);
            } else {
                cacheResult = String.valueOf(ret);
            }
            ContextManager.activeSpan().tag("cache.result", cacheResult);
        }
        ContextManager.stopSpan();
        return ret;
    }

    @Override
    public void handleMethodException(EnhancedInstance objInst, Method method, Object[] allArguments,
                                      Class<?>[] argumentsTypes, Throwable t) {
        AbstractSpan span = ContextManager.activeSpan();
        span.log(t);
    }
}
