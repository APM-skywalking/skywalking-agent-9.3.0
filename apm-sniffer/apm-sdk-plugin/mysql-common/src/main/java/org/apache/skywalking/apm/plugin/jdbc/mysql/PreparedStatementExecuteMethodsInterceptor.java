/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.apache.skywalking.apm.plugin.jdbc.mysql;

import com.ctrip.framework.apollo.ConfigService;
import org.apache.skywalking.apm.agent.core.context.ContextManager;
import org.apache.skywalking.apm.agent.core.context.tag.Tags;
import org.apache.skywalking.apm.agent.core.context.trace.AbstractSpan;
import org.apache.skywalking.apm.agent.core.context.trace.SpanLayer;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.EnhancedInstance;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.InstanceMethodsAroundInterceptor;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.MethodInterceptResult;
import org.apache.skywalking.apm.plugin.jdbc.JDBCDriverInterceptor;
import org.apache.skywalking.apm.plugin.jdbc.PreparedStatementParameterBuilder;
import org.apache.skywalking.apm.plugin.jdbc.SqlBodyUtil;
import org.apache.skywalking.apm.plugin.jdbc.define.StatementEnhanceInfos;
import org.apache.skywalking.apm.plugin.jdbc.trace.ConnectionInfo;

import java.lang.reflect.Method;

public class PreparedStatementExecuteMethodsInterceptor implements InstanceMethodsAroundInterceptor {

    @Override
    public final void beforeMethod(EnhancedInstance objInst, Method method, Object[] allArguments,
                                   Class<?>[] argumentsTypes, MethodInterceptResult result) {
        StatementEnhanceInfos cacheObject = (StatementEnhanceInfos) objInst.getSkyWalkingDynamicField();
        String sql = "";
        if (cacheObject!=null) {
            // 如果statement是制定的则不创建
            sql =cacheObject.getSql();
            if (ConfigService.getAppConfig().getProperty("skywalking.agent.switch.collect.jdbc.sql.filter", "select 1,/* ping */ SELECT 1,XA RECOVER").contains(sql)) {
                return ;
            }
        }
        /**
         * For avoid NPE. In this particular case, Execute sql inside the {@link com.mysql.jdbc.ConnectionImpl} constructor,
         * before the interceptor sets the connectionInfo.
         * When invoking prepareCall, cacheObject is null. Because it will determine procedures's parameter types by executing sql in mysql
         * before the interceptor sets the statementEnhanceInfos.
         * @see JDBCDriverInterceptor#afterMethod(EnhancedInstance, Method, Object[], Class[], Object)
         */
        if (cacheObject != null && cacheObject.getConnectionInfo() != null) {
            ConnectionInfo connectInfo = cacheObject.getConnectionInfo();

            // 如果statement是制定的则不创建
            if (ConfigService.getAppConfig().getProperty("skywalking.agent.switch.collect.jdbc.sql.filter",
                    "select 1,XA RECOVER").contains(cacheObject.getSql())) {
                return;
            }

            AbstractSpan span = ContextManager.createExitSpan(
                    buildOperationName(connectInfo, method.getName(), cacheObject
                            .getStatementName()), connectInfo.getDatabasePeer());
            Tags.DB_TYPE.set(span, connectInfo.getDBType());
            Tags.DB_INSTANCE.set(span, connectInfo.getDatabaseName());

            // 替换 sql中的?
            Tags.DB_STATEMENT.set(span, SqlBodyUtil.limitSqlBodySize(cacheObject.getSql()));
            span.setComponent(connectInfo.getComponent());

            // 是否采集jdbc参数
            if (ConfigService.getAppConfig().getBooleanProperty("skywalking.agent.switch.collect.jdbc.sql", true)) {
                final Object[] parameters = cacheObject.getParameters();
                if (parameters != null && parameters.length > 0) {
                    int maxIndex = cacheObject.getMaxIndex();
                    String parameterString = getParameterString(parameters, maxIndex);
                    Tags.SQL_PARAMETERS.set(span, parameterString);
                }

                String completeSql = replacePlaceholders(cacheObject.getSql(), parameters);

                span.tag("db.sql.completeSql", completeSql);
            }
            SpanLayer.asDB(span);
        }
    }

    private static String replacePlaceholders(String sql, Object[] parameters) {
        if (parameters == null || parameters.length == 0) {
            return sql;
        }

        StringBuilder completeSql = new StringBuilder(sql);
        int index = 1; // 占位符从1开始计数

        for (Object param : parameters) {
            int placeholderIndex = completeSql.indexOf("?", index - 1);
            if (placeholderIndex != -1) {
                String paramStr = convertToSqlValue(param);
                completeSql.replace(placeholderIndex, placeholderIndex + 1, paramStr);
                index = placeholderIndex + paramStr.length();
            }
        }

        return completeSql.toString();
    }

    private static String convertToSqlValue(Object param) {
        if (param instanceof String) {
            return "'" + param.toString().replace("'", "''") + "'"; // 转义单引号
        } else if (param instanceof Integer || param instanceof Long || param instanceof Double || param instanceof Float || param instanceof Byte) {
            return param.toString();
        } else {
            throw new IllegalArgumentException("Unsupported parameter type: " + param.getClass());
        }
    }
    @Override
    public final Object afterMethod(EnhancedInstance objInst, Method method, Object[] allArguments,
                                    Class<?>[] argumentsTypes, Object ret) {
        StatementEnhanceInfos cacheObject = (StatementEnhanceInfos) objInst.getSkyWalkingDynamicField();
        String sql = "";
        if (cacheObject!=null) {
            // 如果statement是制定的则不创建
            sql =cacheObject.getSql();
            if (ConfigService.getAppConfig().getProperty("skywalking.agent.switch.collect.jdbc.sql.filter", "select 1,/* ping */ SELECT 1,XA RECOVER").contains(sql)) {
                return ret ;
            }
        }
        if (cacheObject != null && cacheObject.getConnectionInfo() != null && ContextManager.getSpanId() != -1) {
            ContextManager.stopSpan();
        }
        return ret;
    }

    @Override
    public final void handleMethodException(EnhancedInstance objInst, Method method, Object[] allArguments,
                                            Class<?>[] argumentsTypes, Throwable t) {
        StatementEnhanceInfos cacheObject = (StatementEnhanceInfos) objInst.getSkyWalkingDynamicField();
        if (cacheObject != null && cacheObject.getConnectionInfo() != null) {
            ContextManager.activeSpan().log(t);
        }
    }

    private String buildOperationName(ConnectionInfo connectionInfo, String methodName, String statementName) {
        return connectionInfo.getDBType() + "/JDBC/" + statementName + "/" + methodName;
    }

    private String getParameterString(Object[] parameters, int maxIndex) {
        return new PreparedStatementParameterBuilder()
                .setParameters(parameters)
                .setMaxIndex(maxIndex)
                .build();
    }
}
